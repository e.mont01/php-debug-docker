FROM php:7.3-apache

RUN apt-get update && \
  apt-get install -y curl && \
  pecl install xdebug-2.7.0beta1 && \
  docker-php-ext-enable xdebug

RUN echo 'xdebug.remote_port=9000' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo 'xdebug.remote_connect_back=1' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

EXPOSE 80
