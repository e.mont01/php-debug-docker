<?php
$nameandage = [
  'D' => 24,
  'A' => 44,
  'C' => 34,
  'E' => 55,
  'B' => 21,
  'F' => 48
];

$showArray = function (array $a) {
  foreach ($a as $key => $value) {
    echo "$key = $value", "\n";
  }
};

echo "Original data:\n";
$showArray($nameandage);

ksort ($nameandage);
echo "Sorted by key:\n";
$showArray($nameandage);

$products = [
  [ "name"=>"A", "price"=>4.5 ],
  [ "name"=>"C", "price"=>5.5 ],
  [ "name"=>"D", "price"=>2.5 ],
  [ "name"=>"B", "price"=>2.5 ]
];

function priceCmp($a, $b) {
  if  ( $a['price'] == $b['price'] )
    return 0;
  if  ( $a['price'] < $b['price'] )
    return -1;
  return 1;
}

uasort($products, 'priceCmp');

foreach ($products as $key => $value) {
  print "$key: {$value['price']}\n";
}
